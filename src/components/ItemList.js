import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {itemsFetchData} from '../actions/items';

class ItemList extends Component {
    componentDidMount() {
        this.props.fetchData('http://5af1eee530f9490014ead8c4.mockapi.io/items');
    }
    sorted = []
    makeTree = () => {
        for (let i = 0; i < this.props.items.length; i++) {
            const item = this.props.items[i]
            const parent_id = item.parent_id
            if (parent_id === 0) {
                this.sorted.push(item)
            } else if (parent_id !== this.sorted.parent_id && !this.sorted[0].children) {
                const items = this.props.items.filter(items => items.parent_id === parent_id)
                this.sorted[0].children = items
            } else {
                if(parent_id !== this.sorted[0].children.parent_id && !this.sorted[0].children.children){
                    const items = this.props.items.filter(items => items.parent_id === parent_id)
                    this.sorted[0].children[this.sorted[0].children.length - 1].children = items
                }
            }
        }
    }
    render() {
        if (this.props.items.length === 0) {
            return <div>Loading</div>
        } else {
            this.makeTree()
        }
        return (
                <ul>
                    {this.sorted.map(item => (
                        <li key={item.id}>
                            <span>{item.label}</span>
                            {item.children ? item.children.map(children => (
                                    <ul key={children.id}>
                                        <li key={children.id}>
                                            <span>{children.label}</span>
                                        </li>
                                        {children.children ? children.children.map(children => (
                                                <ul key={children.id}>
                                                    <li key={children.id}>
                                                        <span>{children.label}</span>

                                                    </li>
                                                </ul>
                                            ))
                                            :
                                            null}
                                    </ul>
                                ))
                                :
                                null}
                        </li>
                    ))}
                </ul>
        );
    }
}

ItemList.propTypes = {
    fetchData: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
    return {
        items: state.items,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(itemsFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemList);
